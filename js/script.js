/* 
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
 */

 /* 
 
 Теория 

 1) Для создания элементов можно использовать например document.createElement() / Для добавления: element.append() = element.prepend() = element.before() = element.after();
 2) Для начала элемент который мы хотим удалить нужно найти, const searchElement = document.querySelector('.navigation');
    Он найдет первое совпадение и вернет его, после чего его уже можно удалять
    searchElement.remove();
    элемент удален

3) Метод isertAdjacentElement()
    elem.insertAdjacentElement('beforebegin', newElem) вставляет newElem перед открывающим тегом elem
    elem.insertAdjacentElement('beforeend', newElem) вставляет newElem внутрь после последнего дочернего элемента elem
    elem.insertAdjacentElement('afterbegin', newElem) вставляет newElem сразу после открывающего тега elem
    elem.insertAdjacentElement('afterend', newElem) вставляет newElem сразу после закрывающего тега elem
 
 */

    // Практика

    // 1 

const link = document.createElement('a');
link.innerText = 'Learn more';
link.setAttribute('href', '#')

const searchFooter = document.querySelector('footer');

searchFooter.append(link)


// 2 


const selectElement = document.createElement('select');
selectElement.id = 'rating';

const options = [
    { value: '4', text: '4 Stars' },
    { value: '3', text: '3 Stars' },
    { value: '2', text: '2 Stars' },
    { value: '1', text: '1 Star' },
];

options.forEach(optionData => {
    const optionElement = document.createElement('option');
    optionElement.value = optionData.value;
    optionElement.textContent = optionData.text;
    selectElement.appendChild(optionElement);
});

const mainElement = document.querySelector('main');

const featuresSection = document.querySelector('section.Features');

mainElement.insertBefore(selectElement, featuresSection);